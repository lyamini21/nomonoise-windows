This project works well in WINDOWS jupyter notebook

1.your input files is nomonoise_master/creating_data/Data_Store/warts_file/bwi.warts

2.this .warts file is converted to a csv file using sc_warts2json file.warts > file.csv ......

3.this is already done and the resulting file is stored in nomonoise_master/creating_data/Data_Store/csv_file/bwi.csv

4.the csv file is then the input to create the hops and rtt(s) which is done by running the Creating_Hop_Directory.ipynb and Creating_RTT_directory which will create the files in the rtt and hops folder in the same location.

5.Based on the vantage point in this case bwi.....there are many clients for which details like threshold and means of RTT(S) need to be calculated which can be done by running the code in nomonoise-master/creating_data/snorkel_inputs----Data_Creations_for_20180127.ipynb

This marks the end of all needed input files
Actual Code

in windows manually you will have to set path for SNORKELHOME and USER(used to create a candidate subclass)

SNORKELHOME: where your snorkel-master is stored

USER: any location where you would want the subclass to be stored

1.is there in snorkel-master/tutorials/intro/scripts folder.... - Automated_Script-Elliptic.ipynb code etc

2.this will print the probabilities of a particular RTT belonging to a good class and valid or not...

Errors:

1.Dont download snorkel from github the one which we are using requires snorkel to work on numerical data, so there are changes done to the original code.....

2.Follow the same installation steps as in https://github.com/HazyResearch/snorkel

3.make sure you activate your snorkel environment by using "conda activate snorkel" and then call jupyter notebook

4.After the installation procedure run the "source set_env.sh" (after going to that partcular snorkel-master folder in creating_data) file which will set your SNORKELHOME path

5.similarly also install spacy through this link https://spacy.io/usage (for this make sure your PYTHONPATH is set)

6.in spacy if its not linking the en language then try system library by "{!sys-executable} pip install spacy" in the jupyter notebook so that import failed error is debugged

7.if you get any error in SQL Alchemy then its because there are too many accesses to it so remove those sentences which show error and then run the code once run then paste those lines and run so that the session gets committed

8.in case of any other error then debug line by line using print