For warts file :
(1) Use following command to convert warts to csv file(from creating_data/Data_store/warts_files).
        sc_warts2json file.warts > file.csv
(2) Place this csv file in creating_data/Data_store/csv_files
(3) Run the scripts for creating hop and creating rtt files to generate files in creating_files/data_store/hops and rtt
(4) Run the snorkel_inputs/data_creation.ipynb to generate input for Snorkel.
(5) Once the inputs for Snorkel is ready, you can run the scripts in snorkel-master/tutorials/intro/scripts folder and run the script for the desired the vantage points needed.
(6) Use the Denoise/Output/Top 9 Accuracy.ipynb notebook in Denoise/Output/ directory to plot various graphs.

Make sure you change the location as necessary and if you are using ubuntu then have the snorkel environment set by using
"conda activate snorkel"
the run "jupyter notebook"


